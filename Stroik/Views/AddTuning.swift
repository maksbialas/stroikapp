//
//  AddTuning.swift
//  Stroik
//
//  Created by Maksymilian Białas on 13/10/2020.
//

import SwiftUI

func addTuning(selections: [String], name: String, instrument: Instrument, tunerDisplayState: TunerDisplayState) {
    var newTuningFrequencies: [Float] = []
    let standardTuning: [Float]
    
    switch instrument {
    case .ukulele:
        standardTuning = ukuleleDefaultTunings["Standard"]!
    case .bass:
        standardTuning = bassDefaultTunings["Standard"]!
    default:
        standardTuning = defaultTunings["Standard"]!
    }
    
    for i in 0..<selections.count {
        newTuningFrequencies.append(
            (100*closestFreqency(standardTuning[i], frequencies: freqsForNote[selections[i]]!)).rounded()/100
        )
    }
    
    switch instrument {
    case .ukulele:
        tunerDisplayState.userTuningsUkulele[name] = newTuningFrequencies
    case .bass:
        tunerDisplayState.userTuningsBass[name] = newTuningFrequencies
    default:
        tunerDisplayState.userTunings[name] = newTuningFrequencies
    }
}

func tuningAlreadyExists(name: String, instrument: Instrument, tunerDisplayState: TunerDisplayState) -> Bool {
    switch instrument {
    case .ukulele:
        return tunerDisplayState.userTuningsUkulele[name] != nil || ukuleleDefaultTunings[name] != nil
    case .bass:
        return tunerDisplayState.userTuningsBass[name] != nil || bassDefaultTunings[name] != nil
    default:
        return tunerDisplayState.userTunings[name] != nil || defaultTunings[name] != nil
    }
}


struct AddTuning: View {
    
    @Binding var addingTuning: Bool
    @State var name: String = ""
    
    @StateObject var tunerDisplayState: TunerDisplayState
    let instrument: Instrument
    @State var selections: [String]
    @State var showErrorMessage: Bool = false
    
    var body: some View {
        NavigationView {
            List {
                Section(header: Text("Name")) {
                    TextField("Name", text: $name).disableAutocorrection(true)
                }
                Section(header: Text("Strings"), footer: Text("String 1 has the lowest tone, string 6 has the highest.")) {
                    ForEach(0..<selections.count) { i in
                        Picker("String \(i+1)", selection: $selections[i]) {
                            ForEach(notes, id: \.self) { note in
                                Text(note)
                            }
                        }
                    }
                }
            }
            .listStyle(InsetGroupedListStyle())
            .navigationBarTitle(Text("Add Tuning"), displayMode: .large)
            .navigationBarItems(
                leading:  Button(action: {
                    self.addingTuning = false
                }) {
                    Text("Cancel").fontWeight(.regular)
                },
                trailing: Button(action: {
                    if tuningAlreadyExists(name: name, instrument: instrument, tunerDisplayState: tunerDisplayState) {
                        showErrorMessage = true
                    } else {
                        addTuning(selections: selections, name: name, instrument: instrument, tunerDisplayState: tunerDisplayState)
                        self.addingTuning = false
                    }
                }) {
                    Text("Add")
                }
                .alert(isPresented: self.$showErrorMessage) {
                    Alert(title: Text("Tuning with this name already exist!"), message: Text("Choose another name."), dismissButton: .default(Text("OK")))
                }
                .disabled(name.isEmpty)
            )
        }
    }
}

struct AddTuning_Previews: PreviewProvider {
    static var previews: some View {
        AddTuning(addingTuning: .constant(true), name: "Random Tuning", tunerDisplayState: TunerDisplayState(), instrument: .guitar, selections: getInstrumentsStandardTuning(instrument: .guitar))
    }
}
