//
//  FFTPlot.swift
//  Stroik
//
//  Created by Maksymilian Białas on 18/11/2020.
//

import SwiftUI

struct PlotShapeAnimatable: Shape {
    var data: AnimatableVector
    
    var animatableData: AnimatableVector {
        get { data }
        set { self.data = newValue }
    }
    
    func path(in rect: CGRect) -> Path {
        Path { path in
            let width = (rect.width-4.0) / CGFloat(data.values.count-1)
            path.move(to: CGPoint(x: 4.0, y: 0.0))
            
            let maximum = data.values.max()!
            let power = 1.0
            
            for (n, point) in data.values.enumerated() {
                path.addLine(to: CGPoint(x: 4.0+CGFloat(n)*width,
                                         y: maximum.isZero ? CGFloat(pow(point, power)) : rect.height * CGFloat(pow(point, power) / pow(maximum*1.3, power)))
                )
            }
            
            path = path.offsetBy(dx: -2.0, dy: 3.0)
        }
    }
}

struct FFTPlotAnimatable: View {
    
    @ObservedObject var data: DetectionResult
    
    var body: some View {
        PlotShapeAnimatable(data: AnimatableVector(
            values: ([0.0] + data.fftResult[0..<12] + [0.0]).map{ Double($0) }
        ))
        .stroke(Color.blue, style: StrokeStyle(lineWidth: 2, lineCap: .round, lineJoin: .round))
        .animation(Animation.linear(duration: 0.3))
        .rotationEffect(.degrees(180), anchor: .center)
        .rotation3DEffect(.degrees(180), axis: (x: 0, y: 1, z: 0))
        .drawingGroup()
    }
}
//
//struct FFTChart_Previews: PreviewProvider {
//    static var previews: some View {
//        FFTPlot()
//    }
//}
