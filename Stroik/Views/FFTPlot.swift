//
//  FFTPlot.swift
//  Stroik
//
//  Created by Maksymilian Białas on 18/11/2020.
//

import SwiftUI

struct PlotShape: Shape {

    var data: AnimatableVector
    var quadCornerRadius: CGFloat
    
    var animatableData: AnimatableVector {
        get { data }
        set { self.data = newValue }
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        let width = rect.width / (CGFloat(data.values.count) + 1.0)
        let power: Float = 0.75
        
        var xs = [CGFloat](repeating: 0.0, count: data.values.count)
        var ys = [CGFloat](repeating: 0.0, count: data.values.count)
        var zRatios = [CGFloat](repeating: 0.0, count: data.values.count+1)
        let maximum = Float(data.values.max()!)
        
        path.move(to: CGPoint(x: 2.0, y: 0))
        
        for (n, pointDouble) in data.values.enumerated() {
            xs[n] = CGFloat(CGFloat(n+1) * width)
            
            let point = Float(pointDouble)
            ys[n] = maximum.isZero ? CGFloat(pow(point, power)) : rect.height * CGFloat(pow(point, power) / pow(maximum*1.6, power))
            
        }
        
        for n in 0..<data.values.count {
            if n == 0 {
                zRatios[0] = quadCornerRadius / sqrt(pow(xs[0],2) + pow(ys[0],2))
            } else {
                zRatios[n] = quadCornerRadius / sqrt(pow(xs[n] - xs[n-1], 2) + pow(ys[n] - ys[n-1], 2))
            }
        }
        
        zRatios[data.values.count] = quadCornerRadius / sqrt(pow(rect.width - xs.last!, 2) + pow(ys.last!-2.0, 2))
        
        for n in 0..<data.values.count {
            
            if n == 0 {
                path.addLine(to: CGPoint(
                    x: xs[n] - zRatios[n]*width,
                    y: ys[n] - zRatios[n]*ys[n]
                ))
            } else {
                path.addQuadCurve(
                    to: CGPoint(
                        x: xs[n-1] + zRatios[n]*width,
                        y: ys[n-1] + zRatios[n]*(ys[n] - ys[n-1])
                    ),
                    control: CGPoint(
                        x: xs[n-1],
                        y: ys[n-1]
                    )
                )
                path.addLine(to: CGPoint(
                    x: xs[n] - zRatios[n]*width,
                    y: ys[n] - zRatios[n]*(ys[n] - ys[n-1])
                ))
            }
        }
        
        path.addQuadCurve(
            to: CGPoint(
                x: xs.last! + zRatios.last!*width,
                y: ys.last! - zRatios.last!*(ys.last!)
            ),
            control: CGPoint(
                x: xs.last!,
                y: ys.last!
            )
        )
        
        path.addLine(to: CGPoint(x: rect.width-2.0, y: 0))
        return path.offsetBy(dx: 0, dy: 3.0)
    }
}

struct FFTPlot: View {
    
    @ObservedObject var data: DetectionResult
    
    var body: some View {
        ZStack{
//        Color.red
            PlotShape(data: AnimatableVector(values: (data.fftResult[0..<12]).map{ Double($0) }), quadCornerRadius: 4.0)
            .stroke(Color.blue, style: StrokeStyle(lineWidth: 2, lineCap: .round))
            .animation(Animation.linear(duration: 0.3))
            .rotationEffect(.degrees(180), anchor: .center)
            .rotation3DEffect(.degrees(180), axis: (x: 0, y: 1, z: 0))
            .drawingGroup()
        }
    }
}
//
//struct FFTChart_Previews: PreviewProvider {
//    static var previews: some View {
//        FFTPlot()
//    }
//}
