//
//  SettingsView.swift
//  Stroik
//
//  Created by Maksymilian Białas on 12/10/2020.
//

import SwiftUI

var instruments: [Instrument] = Instrument.allCases
var displayModes: [FrequencyDisplayMode] = FrequencyDisplayMode.allCases

func modeToDesc(_ mode: Int) -> String {
    switch mode {
    case 0:
        return "Cents"
    case 1:
        return "Hertz (0.1 Hz)"
    case 2:
        return "Hertz (0.01 Hz)"
    default:
        return " "
    }
}

struct SettingsView: View {
    
    @State var addingTuning = false
    @Binding var showingSettings: Bool
    @StateObject var tunerDisplayState: TunerDisplayState
    @State var instrument = "Guitar"
    @StateObject var purchased = PurchasedProducts.shared
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    
                    Section(header: Text("Instrument")) {
                        Picker("Instrument", selection: $tunerDisplayState.instrument) {
                            ForEach(instruments, id: \.self) { instrument in
                                Text(instrument.rawValue)
                            }
                        }
                        .disabled(tunerDisplayState.chromaticMode || !purchased.proFeatures)
                        .pickerStyle(SegmentedPickerStyle())
                    }
                    
                    Section(header: Text("Pitch detection"), footer: Text("Chromatic mode will automaticly recognize all twelve pitches.")) {
                        Toggle(isOn: $tunerDisplayState.chromaticMode) {
                            Text("Chromatic Mode")
                                .foregroundColor(purchased.proFeatures ? .primary : .secondary)
                        }
                        .disabled(!purchased.proFeatures)
                        
                        Picker("Display Frequency in", selection: $tunerDisplayState.displayFrequency) {
                            ForEach(displayModes, id: \.self) { mode in
                                Text(modeToDesc(mode.rawValue))
                            }
                        }
                        .disabled(!purchased.proFeatures)
                    }
                    

                    Section (header: Text("Custom tunings"), footer: Text("Swipe left to delete.")) {
                        
                        ForEach(Array(tunerDisplayState.getUserTuningsForCurrentInstrument().keys).sorted{$0.localizedCompare($1) == .orderedAscending}, id: \.self) { tuning in
                            HStack {
                                Text(tuning)
                                Spacer()
                                Text(notesFromFrequencies(tunerDisplayState.getUserTuningsForCurrentInstrument()[tuning]!)).font(.subheadline).foregroundColor(.secondary)
                            }
                        }
                        .onDelete(perform: { i in
                            switch tunerDisplayState.instrument {
                            case .ukulele:
                                let keyDeleted = Array(tunerDisplayState.userTuningsUkulele.keys).sorted{$0.localizedCompare($1) == .orderedAscending}[i.map({$0})[0]]
                                
                                if tunerDisplayState.currentTuningUkulele == keyDeleted {
                                    tunerDisplayState.currentTuningUkulele = "Standard"
                                }
                                tunerDisplayState.userTuningsUkulele[keyDeleted] = nil
                            case .bass:
                                let keyDeleted = Array(tunerDisplayState.userTuningsBass.keys).sorted{$0.localizedCompare($1) == .orderedAscending}[i.map({$0})[0]]
                                
                                if tunerDisplayState.currentTuningBass == keyDeleted {
                                    tunerDisplayState.currentTuningBass = "Standard"
                                }
                                tunerDisplayState.userTuningsBass[keyDeleted] = nil
                            default:
                                let keyDeleted = Array(tunerDisplayState.userTunings.keys).sorted{$0.localizedCompare($1) == .orderedAscending}[i.map({$0})[0]]
                                
                                if tunerDisplayState.currentTuning == keyDeleted {
                                    tunerDisplayState.currentTuning = "Standard"
                                }
                                tunerDisplayState.userTunings[keyDeleted] = nil
                            }
                        })
                        Button(action: {
                            self.addingTuning.toggle()
                        }){
                            Label("Add tuning", systemImage: "plus.circle")
                        }
                    }
                    .disabled(tunerDisplayState.chromaticMode || !purchased.proFeatures)
                    
                    if !purchased.proFeatures {
                        Button(action: { StoreObserver.shared.requestPayment(product: StoreObserver.shared.products[0]) }) {
                            Text("Upgrade to Pro")
                        }
                        Button(action: {
                            StoreObserver.shared.restorePurchase()
                        }) {
                            Text("Restore purchase")
                        }
                    }
                }
                .listStyle(InsetGroupedListStyle())
                .navigationBarTitle(Text("Settings"), displayMode: .large)
                .navigationBarItems(trailing:  Button(action: {
                    self.showingSettings = false
                }) {
                    Text("Done").bold()
                })
            }
        }.sheet(isPresented: $addingTuning) {
            if purchased.proFeatures {
                AddTuning(addingTuning: self.$addingTuning, tunerDisplayState: tunerDisplayState, instrument: tunerDisplayState.instrument, selections: getInstrumentsStandardTuning(instrument: tunerDisplayState.instrument))
            }
        }
    }
}

struct Settings_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(showingSettings: .constant(true), tunerDisplayState: TunerDisplayState())
    }
}
