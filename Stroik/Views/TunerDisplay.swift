//
//  TunerDisplay.swift
//  Stroik
//
//  Created by Maksymilian Białas on 06/10/2020.
//

import SwiftUI


struct TunerDisplay: View {
    
    @StateObject var detectionResult: DetectionResult
    @StateObject var tunerDisplayState: TunerDisplayState
    
    var body: some View {
        VStack {
            Spacer()
            Text(tunerDisplayState.getNoteName(frequency: detectionResult.result))
                .font(.custom("Futura", size: 64))
                .foregroundColor(detectionResult.result == 0.0 ? .secondary : .primary)
            
            Spacer()
            Gauge(detectionResult: detectionResult, tunerDisplayState: tunerDisplayState)
            
            switch  tunerDisplayState.displayFrequency {
            case .tenth:
                Text(detectionResult.result == 0.0 ? "Play a note!" : String(format: "%.1f", detectionResult.result)).padding(.top).font(.custom("Futura", size: 18))
            case .hundredth:
                Text(detectionResult.result == 0.0 ? "Play a note!" : String(format: "%.2f", detectionResult.result)).padding(.top).font(.custom("Futura", size: 18))
            default:
                Text(detectionResult.result == 0.0 ? "Play a note!" : "\(tunerDisplayState.getCents(frequency: detectionResult.result))").padding(.top).font(.custom("Futura", size: 18))
            }
            
            
//            Spacer()
            FFTPlot(data: audioManage.detectionResult).frame(width: 80, height: 60, alignment: .center).padding(.top)
            Spacer()
            if (tunerDisplayState.chromaticMode) {
                Text("Chromatic Mode")
                Spacer()
            } else {
                HStack {
                    Spacer()
                    ForEach(tunerDisplayState.tuningFrequencies , id: \.self) { frequency in
                        Text(noteFromFrequency(frequency))
                            .foregroundColor(closestFreqency(detectionResult.result, frequencies: tunerDisplayState.tuningFrequencies) == (frequency*20).rounded()/20 ? .primary : .secondary)
                            .font(.custom("Futura", size: 22))
                    }
                    Spacer()
                }
                
                // DO ZMIANY NA PEWNO
                switch  tunerDisplayState.instrument {
                case .ukulele:
                    Picker ("\(tunerDisplayState.currentTuningUkulele)", selection: $tunerDisplayState.currentTuningUkulele) {
                        Section {
                            ForEach(Array(tunerDisplayState.userTuningsUkulele.keys).sorted{$0.localizedCompare($1) == .orderedDescending}, id: \.self) { tuning in
                                Text(tuning)
                            }
                        }
                        Section {
                            ForEach(ukuleleTuningsOrdered, id: \.self) { tuning in
                                Text(tuning)
                            }
                        }
                    }.pickerStyle(MenuPickerStyle())
                    .animation(.none)
                    .padding(.top)
                case .bass:
                    Picker ("\(tunerDisplayState.currentTuningBass)", selection: $tunerDisplayState.currentTuningBass) {
                        Section {
                            ForEach(Array(tunerDisplayState.userTuningsBass.keys).sorted{$0.localizedCompare($1) == .orderedDescending}, id: \.self) { tuning in
                                Text(tuning)
                            }
                        }
                        Section {
                            ForEach(bassTuningsOrdered, id: \.self) { tuning in
                                Text(tuning)
                            }
                        }
                    }.pickerStyle(MenuPickerStyle())
                    .animation(.none)
                    .padding(.top)
                default:
                    Picker ("\(tunerDisplayState.currentTuning)", selection: $tunerDisplayState.currentTuning) {
                        Section {
                            ForEach(Array(tunerDisplayState.userTunings.keys).sorted{$0.localizedCompare($1) == .orderedDescending}, id: \.self) { tuning in
                                Text(tuning)
                            }
                        }
                        Section {
                            ForEach(tuningsOrdered, id: \.self) { tuning in
                                Text(tuning)
                            }
                        }
                    }.pickerStyle(MenuPickerStyle())
                    .animation(.none)
                    .padding(.top)
                }
            }
            
            Spacer()
        }
    }
}

struct TunerDisplay_Previews: PreviewProvider {
    static var previews: some View {
        TunerDisplay(detectionResult: DetectionResult(result: 329.7), tunerDisplayState: TunerDisplayState())
    }
}
