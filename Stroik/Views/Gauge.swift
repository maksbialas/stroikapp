//
//  Gauge.swift
//  Stroik
//
//  Created by Maksymilian Białas on 08/10/2020.
//

import SwiftUI

struct Gauge: View {
    
    @ObservedObject var detectionResult: DetectionResult
    @ObservedObject var tunerDisplayState: TunerDisplayState
    
    var body: some View {
        ZStack {
            if UIDevice.current.localizedModel == "iPhone" {
                HStack (spacing: UIScreen.main.bounds.size.width > 350 ? 16 : 12) {
                    ForEach (1..<20) { i in
                        Rectangle()
                            .fill(Color.primary)
                            .frame(
                                width: i % 10 == 0 ? 2 : 1,
                                height: i % 10 == 0 ? 90 : (i % 5 == 0 ? 40 : 20)
                            )
                    }
                }
            } else if UIDevice.current.localizedModel == "iPad" {
                HStack (spacing: 32) {
                    ForEach (1..<20) { i in
                        Rectangle()
                            .fill(Color.primary)
                            .frame(
                                width: i % 10 == 0 ? 4 : 2,
                                height: i % 10 == 0 ? 180 : (i % 5 == 0 ? 80 : 40)
                            )
                    }
                }
            }
            
            if UIDevice.current.localizedModel == "iPhone" {
                Rectangle()
                    .fill(tunerDisplayState.getIndicatorColor(frequency: detectionResult.result))
                    .animation(.easeOut(duration: 0.3))
                    .frame(width: 4, height: 60)
                    .offset(x: CGFloat(tunerDisplayState.getGaugePosition(frequency: detectionResult.result)))
                    .animation(.linear(duration: 2))
            } else if UIDevice.current.localizedModel == "iPad" {
                Rectangle()
                    .fill(tunerDisplayState.getIndicatorColor(frequency: detectionResult.result))
                    .animation(.easeOut(duration: 0.3))
                    .frame(width: 8, height: 120)
                    .offset(x: CGFloat(tunerDisplayState.getGaugePosition(frequency: detectionResult.result)))
                    .animation(.linear(duration: 2))
            }
        }
    }
}

struct Gauge_Previews: PreviewProvider {
    static var previews: some View {
        Gauge(detectionResult: DetectionResult(result: 329.6), tunerDisplayState: TunerDisplayState())
    }
}
