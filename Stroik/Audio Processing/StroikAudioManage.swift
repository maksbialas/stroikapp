//
//  StroikAudioManage.swift
//  Stroik
//
//  Created by Maksymilian Białas on 22/09/2020.
//

import AVFoundation

typealias RecordedStreamProcessingCallback = (_ numberOfSamples: Int, _ samples: [Float], _ detectionResult: DetectionResult) -> Void

class StroikAudioManage: NSObject, ObservableObject {
    let audioSession = AVAudioSession.sharedInstance()
    private(set) var audioUnit: AudioUnit!
    
    private var samplingFrequency: Float
    var detectionResult: DetectionResult
    private var bufferSize: Double
    private var callback: RecordedStreamProcessingCallback!
    
    private let outputBus: UInt32 = 0
    private let inputBus: UInt32 = 1
    
    
    
    private let inputCallback: AURenderCallback = { (inRefCon, ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, ioData) -> OSStatus in
        
        // retrieve provided pointer and cast it to StroikAudioManage object
        let audioManage = Unmanaged<StroikAudioManage>.fromOpaque(inRefCon).takeUnretainedValue()
        var status: OSStatus = 0
        
        // setup audio buffer
        let audioBuffer = AudioBuffer(
            mNumberChannels: 1,
            mDataByteSize: 4,
            mData: nil
        )
        
        // create buffer list containing audio buffer
        var bufferList = AudioBufferList(mNumberBuffers: UInt32(1), mBuffers: (audioBuffer))
        
        // initiate audio rendering to audio buffer
        status = AudioUnitRender(
            audioManage.audioUnit,
            ioActionFlags,
            inTimeStamp,
            inBusNumber,
            inNumberFrames,
            &bufferList
        )
        
        var samples: [Float] = []
        
        // create pointer to buffer and append its content to Float array
        let pointer = bufferList.mBuffers.mData?.assumingMemoryBound(to: Float.self)
        samples.append(contentsOf: UnsafeBufferPointer(start: pointer, count: Int(inNumberFrames)))
        
        audioManage.callback(Int(inNumberFrames), samples, audioManage.detectionResult)
        
        return status
    }
    
    init(onRecordedCallback callback: @escaping RecordedStreamProcessingCallback, samplingFrequency fs: Float, detectionResult res: DetectionResult, bufferSize: Double) {
        self.callback = callback
        self.samplingFrequency = fs
        self.detectionResult = res
        self.bufferSize = bufferSize
    }
    
    func startRecording() {
        AudioOutputUnitStart(self.audioUnit)
    }
    
    func stopRecording() {
        AudioOutputUnitStop(self.audioUnit)
    }
    
    func setupAudioUnit() {
        
        // setup AudioSession
        do {
            try audioSession.setCategory(.record)
            try audioSession.setMode(.measurement)
            try audioSession.setPreferredSampleRate(Double(samplingFrequency))
            try audioSession.setPreferredIOBufferDuration(bufferSize/Double(samplingFrequency))
        } catch {
            print("Audio session error: \(error)")
        }
        
        audioSession.requestRecordPermission { granted in
            if !granted { print("Permission not granted!") }
        }
        
        
        // create AudioComponent description
        var componentDesc = AudioComponentDescription(
            componentType: kAudioUnitType_Output,
            componentSubType: kAudioUnitSubType_RemoteIO,
            componentManufacturer: kAudioUnitManufacturer_Apple,
            componentFlags: 0,
            componentFlagsMask: 0
        )
        
        // find AudioComponent matching description
        let audioComponent: AudioComponent? = AudioComponentFindNext(nil, &componentDesc)
        
        guard audioComponent != nil else {
            print("Did not found AudioComponent matching description")
            return
        }
        
        // create instance of audio component in self.audioUnit
        AudioComponentInstanceNew(audioComponent!, &self.audioUnit)
        
        // output is enabled by default
        // input is disabled by default, enabling it on input bus here by AudioUnitSetProperty
        var enableInput: UInt32 = 1
        
        AudioUnitSetProperty(
            audioUnit,
            kAudioOutputUnitProperty_EnableIO,
            kAudioUnitScope_Input,
            inputBus,
            &enableInput,                         // enable input
            UInt32(MemoryLayout<UInt32>.size)
        )
        
        let bytesPerSample = UInt32(MemoryLayout<Int32>.size)
        
        
        // setup stream format
        var streamFormatDesc:AudioStreamBasicDescription = AudioStreamBasicDescription(
            mSampleRate: Float64(samplingFrequency),
            mFormatID: kAudioFormatLinearPCM,
            mFormatFlags: kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved,
            mBytesPerPacket: bytesPerSample,
            mFramesPerPacket: 1,
            mBytesPerFrame: bytesPerSample,
            mChannelsPerFrame: 1,
            mBitsPerChannel: 8 * bytesPerSample,
            mReserved: 0
        )
        
        AudioUnitSetProperty(
            audioUnit,
            kAudioUnitProperty_StreamFormat,
            kAudioUnitScope_Output,
            inputBus,
            &streamFormatDesc,
            UInt32(MemoryLayout<AudioStreamBasicDescription>.size)
        )
        
        //callback setup
        var inputCallbackStruct = AURenderCallbackStruct(
            inputProc: inputCallback,
            inputProcRefCon: UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())
        )
        
        AudioUnitSetProperty(
            audioUnit,
            kAudioOutputUnitProperty_SetInputCallback,
            kAudioUnitScope_Global,
            inputBus,
            &inputCallbackStruct,
            UInt32(MemoryLayout<AURenderCallbackStruct>.size)
        )
        
        do { try self.audioSession.setActive(true) } catch { print("Audio session error: \(error)") }
        AudioUnitInitialize(self.audioUnit)
    }
}
