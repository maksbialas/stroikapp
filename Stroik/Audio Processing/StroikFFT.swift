//
//  StroikFFT.swift
//  Stroik
//
//  Created by Maksymilian Białas on 16/09/2020.
//

import Accelerate

class StroikFFT {
    
    static let shared = StroikFFT(length: fftSize, samplingRate: 48000)
    
    let fs: Float
    let fftSetUp: vDSP.FFT<DSPSplitComplex>
    var n: Int
    let window: [Float]
    
    var forwardInputReal : [Float]
    var forwardInputImag : [Float]
    var forwardOutputReal : [Float]
    var forwardOutputImag : [Float]
    
    var forwardOutput: DSPSplitComplex?
    var magnitudes: [Float]
//    var filter: [Float]
    
    
    var log2n: UInt {
        vDSP_Length(log2(Float(n)))
    }
    
    init(length n: Int, samplingRate fs: Float) {
        self.n = n
        self.fs = fs
        self.fftSetUp = vDSP.FFT(log2n: vDSP_Length(log2(Float(n))), radix: .radix2, ofType: DSPSplitComplex.self)!
        self.window = vDSP.window(ofType: Float.self,
                                 usingSequence: .hamming,
                                 count: n,
                                 isHalfWindow: false)
        
        self.forwardInputReal = [Float](repeating: 0,
                                       count: n/2)
        self.forwardInputImag = [Float](repeating: 0,
                                       count: n/2)
        self.forwardOutputReal = [Float](repeating: 0,
                                        count: n/2)
        self.forwardOutputImag = [Float](repeating: 0,
                                        count: n/2)
        
        self.magnitudes = [Float](repeating: 0.0, count: n/2)
        
    }
    
    func performFFT(signal audio: [Float]) -> [Float] {
        
        let signal = audio

//        signal = vDSP.multiply(signal, window)

        forwardInputReal.withUnsafeMutableBufferPointer { forwardInputRealPtr in
            forwardInputImag.withUnsafeMutableBufferPointer { forwardInputImagPtr in
                forwardOutputReal.withUnsafeMutableBufferPointer { forwardOutputRealPtr in
                    forwardOutputImag.withUnsafeMutableBufferPointer { forwardOutputImagPtr in
                        
                        // 1: Create a `DSPSplitComplex` to contain the signal.
                        var forwardInput = DSPSplitComplex(realp: forwardInputRealPtr.baseAddress!,
                                                           imagp: forwardInputImagPtr.baseAddress!)
                        
                        // 2: Convert the real values in `signal` to complex numbers.
                        signal.withUnsafeBytes {
                            vDSP.convert(interleavedComplexVector: [DSPComplex]($0.bindMemory(to: DSPComplex.self)),
                                         toSplitComplexVector: &forwardInput)
                        }
                        
                        // 3: Create a `DSPSplitComplex` to receive the FFT result.
                        forwardOutput = DSPSplitComplex(realp: forwardOutputRealPtr.baseAddress!,
                                                            imagp: forwardOutputImagPtr.baseAddress!)
                        
                        // 4: Perform the forward FFT.
                        fftSetUp.forward(input: forwardInput,
                                         output: &forwardOutput!)
                    }
                }
            }
        }
        
        vDSP_zvmags(&forwardOutput!, 1, &magnitudes, 1, UInt(n/2))
    
        return magnitudes
    }
}
