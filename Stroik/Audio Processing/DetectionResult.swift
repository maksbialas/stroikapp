//
//  DetectionResult.swift
//  Stroik
//
//  Created by Maksymilian Białas on 08/10/2020.
//

import Foundation

class DetectionResult: ObservableObject {
    @Published var result: Float
    var resultArr = [Float](repeating: 0.0, count: resultArrSize)
    @Published var fftResult: [Float] 
    
    init(result: Float) {
        self.result = result
        self.fftResult = [Float](repeating: 0.0, count: fftSize)
    }
    
    init(result: Float, fftResult: [Float]) {
        self.result = result
        self.fftResult = fftResult
    }
}
