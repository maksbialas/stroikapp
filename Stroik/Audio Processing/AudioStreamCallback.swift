//
//  AudioStreamCallback.swift
//  Stroik
//
//  Created by Maksymilian Białas on 08/10/2020.
//

import Foundation
import CoreML
import UIKit

let audioCallback: RecordedStreamProcessingCallback = { (numberOfSamples, samples, detectionResult) in
    
    let yinResult = yin(signal: samples)
    
    let samplesDownsampled = Array(samples[0..<fftSize])
    DispatchQueue.main.async { detectionResult.fftResult = StroikFFT.shared.performFFT(signal: samplesDownsampled) }
    
    
    if yinResult < 20 {
        DispatchQueue.main.async {
            detectionResult.result = 0.0
        }
        return
    } else {
        detectionResult.resultArr.removeFirst()
        detectionResult.resultArr.append(yinResult)
    }
    
    let average = detectionResult.resultArr.avg()
    var chosen: [Float] = []
    
    for res in detectionResult.resultArr {
        if abs(res - average) < 1 {
            chosen.append(res)
        }
    }
    
    DispatchQueue.main.async {
        detectionResult.result = (chosen.count > resultArrSize/2 ? chosen.avg() : 0.0)
    }
}
