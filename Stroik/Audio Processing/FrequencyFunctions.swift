//
//  Frequencies.swift
//  Stroik
//
//  Created by Maksymilian Białas on 08/10/2020.
//

import Foundation


// można zrobić logarytmicznie
func closestFreqency(_ f: Float, frequencies freqs: [Float]) -> Float {
    return f == 0.0 ? 0.0 : (freqs.min( by: { abs($0 - f) < abs($1 - f) } )!*20).rounded()/20
}

func closestFreqency(_ f: Float) -> Float {
    if f == 0.0 { return 0.0 }
    var noteIndex = 12 * log2(f/440.0)
    
    noteIndex = noteIndex.rounded()
    
    return (440.0*pow(2, noteIndex/12)*20).rounded()/20
}

func noteFromFrequency (_ f: Float) -> String {
    
    if f == 0.0 { return "-" }
    
    let notes = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]
    var noteIndex = 12 * log2(f/440.0)
    
    noteIndex = noteIndex.rounded()
    
    if noteIndex < 0 {
        let index = 12 - Int(-noteIndex) % 12
        return notes[index == 12 ? 0 : index]
    } else {
        return notes[Int(noteIndex) % 12]
    }
    
}

func notesFromFrequencies (_ freqs: [Float]) -> String {
    var result = ""
    for f in freqs {
        result.append(" \(noteFromFrequency(f))")
    }
    return result
}

@propertyWrapper
struct GaugePosition {
    private var number: Float
    init() { self.number = 0.0 }
    var wrappedValue: Float {
        get { return number }
        set { number = max(min(newValue, 154), -154) }
    }
}

@propertyWrapper
struct DisplayedNote {
    private var note: String
    init() { self.note = "–" }
    var wrappedValue: String {
        get { return note }
        set { note = newValue == "-" ? note : newValue }
    }
}

var freqsForNote: [String: [Float]] {
    var dict = [String: [Float]]()
    
    for i in 0..<notes.count {
        let noteLowFreq: Float = 27.5 * pow(2.0, Float(i)/12.0)
        dict[notes[i]] = [noteLowFreq, noteLowFreq*2, noteLowFreq*4, noteLowFreq*8]
    }
    
    return dict
}

func getInstrumentsStandardTuning(instrument: Instrument) -> [String] {
    switch instrument {
    case .ukulele:
        return ["G", "C", "E", "A"]
    case .bass:
        return ["E", "A", "D", "G"]
    default:
        return ["E", "A", "D", "G", "B", "E"]
    }
}
