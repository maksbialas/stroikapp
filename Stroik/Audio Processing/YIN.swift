//
//  YIN.swift
//  Stroik
//
//  Created by Maksymilian Białas on 22/10/2020.
//

import Accelerate

func yin(signal: [Float]) -> Float {
    let n = Int(signal.count/2)
    var result = [Float](repeating: 0, count: n)
    
    var tempBuffer = [Float](repeating:0.0, count: n)
    var tempBufferSquared = [Float](repeating:0.0, count: n)
    let len = vDSP_Length(n)
    var sum: Float = 0.0
    var runningSum: Float = 0
    
    for tau in 0..<n {
        let signalHalfAdvancedByTau = Array(signal[tau..<tau+n])
        vDSP_vsub(signal, 1, signalHalfAdvancedByTau, 1, &tempBuffer, 1, len)
        vDSP_vsq(tempBuffer, 1, &tempBufferSquared, 1, len)
        vDSP_sve(tempBufferSquared, 1, &sum, len)
        
        if tau == 0 {
            result[tau] = 1
        } else {
            runningSum += sum
            if runningSum == 0 {
                result[tau] = 1
            } else {
                result[tau] = Float(tau) * sum / runningSum
            }
        }
    }
    
    let tau = absoluteThreshold(yinResult: result, withThreshold: 0.1)
    var f0: Float
    
    if tau != 0 {
        let interpolatedTau = parabolicInterpolation(yinResult: result, tau: tau)
        f0 = Float(48000/interpolatedTau)
    } else {
        f0 = 0.0
    }
    
    return f0
}

func absoluteThreshold(yinResult: [Float], withThreshold threshold: Float) -> Int {
    
    var tau = 2
    while tau < yinResult.count {
        if yinResult[tau] < threshold {
            while (tau + 1) < yinResult.count && yinResult[tau + 1] < yinResult[tau] {
                tau += 1
            }
            return tau
        }
        tau += 1
    }
    
    return 0
}


func parabolicInterpolation(yinResult: [Float], tau: Int) -> Float {
    guard tau != yinResult.count else {
        return Float(tau)
    }
    
    var tauInterpolated: Float = 0
    
    if tau > 0  && tau < yinResult.count - 1 {
        let s0 = yinResult[tau - 1]
        let s1 = yinResult[tau]
        let s2 = yinResult[tau + 1]
        
        var interpolation = (s2 - s0) / (2.0 * (2.0 * s1 - s2 - s0))
        
        if abs(interpolation) > 1 {
            interpolation = 0
        }
        tauInterpolated = Float(tau) + interpolation
    } else {
        tauInterpolated = Float(tau)
    }
    
    return abs(tauInterpolated)
}
