//
//  StoreObserver.swift
//  Stroik
//
//  Created by Maksymilian Białas on 30/10/2020.
//

import Foundation
import StoreKit


class StoreObserver: NSObject, SKPaymentTransactionObserver {
    
    static let shared = StoreObserver()
    var products = [SKProduct]()
    var request: SKProductsRequest!
    var productIdentifiers: [String] = []
    
    func startObserving() {
        SKPaymentQueue.default().add(self)
    }
    
    func stopObserving() {
        SKPaymentQueue.default().remove(self)
    }
    
    func readProducts() {
        guard let url = Bundle.main.url(forResource: "Products", withExtension: "plist") else { fatalError("Unable to resolve url for in the bundle.") }
        do {
            let data = try Data(contentsOf: url)
            self.productIdentifiers = try PropertyListSerialization.propertyList(from: data, options: .mutableContainersAndLeaves, format: nil) as? [String] ?? []
            
        } catch let error as NSError {
            print("\(error.localizedDescription)")
        }
    }
    
    func requestPayment(product: SKProduct) {
        if (SKPaymentQueue.canMakePayments()) {
            let payment = SKMutablePayment(product: product)
            payment.quantity = 1
            SKPaymentQueue.default().add(payment)
        }
    }
        
    func restorePurchase() {
        if (SKPaymentQueue.canMakePayments()) {
            SKPaymentQueue.default().restoreCompletedTransactions()
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            // Call the appropriate custom method for the transaction state.
            case .purchasing:
                print("Purchasing")
                break
            case .deferred:
                print("Deferred")
            case .failed:
                print("Failed")
                SKPaymentQueue.default().finishTransaction(transaction)
            case .purchased:
                print("Purchased")
                PurchasedProducts.shared.proFeatures = true
                SKPaymentQueue.default().finishTransaction(transaction)
            case .restored:
                print("Restored")
                PurchasedProducts.shared.proFeatures = true
                SKPaymentQueue.default().finishTransaction(transaction)
            // For debugging purposes.
            @unknown default: print("Unexpected transaction state \(transaction.transactionState)")
            }
        }
    }
    
    override init() {
        super.init()
    }
}

extension StoreObserver: SKProductsRequestDelegate {
    
    func validate(productIdentifiers: [String]) {
        let productIdentifiers = Set(productIdentifiers)
        
        request = SKProductsRequest(productIdentifiers: productIdentifiers)
        request.delegate = self
        request.start()
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if !response.products.isEmpty {
            products = response.products
        }
        
        for invalidIdentifier in response.invalidProductIdentifiers {
            print("Invalid product: ", invalidIdentifier)
        }
    }
}

class PurchasedProducts: ObservableObject {
    static let shared = PurchasedProducts()
    @Published var proFeatures: Bool {
        didSet {
            UserDefaults.standard.set(proFeatures, forKey: "ProFeatures")
        }
    }
    
    init() {
        self.proFeatures = UserDefaults.standard.bool(forKey: "ProFeatures")
    }
}

