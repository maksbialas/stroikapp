//
//  TunerDisplayState.swift
//  Stroik
//
//  Created by Maksymilian Białas on 13/10/2020.
//

enum Instrument: String, CaseIterable {
    case guitar = "Guitar"
    case ukulele = "Ukulele"
    case bass = "Bass"
}

enum FrequencyDisplayMode: Int, CaseIterable {
    case cents = 0
    case tenth = 1
    case hundredth = 2
}

import Foundation
import SwiftUI
class TunerDisplayState: ObservableObject {
    
    @Published var chromaticMode: Bool {
        didSet {
            UserDefaults.standard.set(chromaticMode, forKey: "ChromaticMode")
        }
    }
    
    @Published var currentTuning: String {
        didSet {
            UserDefaults.standard.set(currentTuning, forKey: "CurrentTuning")
        }
    }
    
    @Published var currentTuningUkulele: String {
        didSet {
            UserDefaults.standard.set(currentTuningUkulele, forKey: "CurrentTuningUkulele")
        }
    }
    
    @Published var currentTuningBass: String {
        didSet {
            UserDefaults.standard.set(currentTuningBass, forKey: "CurrentTuningBass")
        }
    }
    
    @Published var userTunings: [String: [Float]] {
        didSet {
            UserDefaults.standard.set(userTunings, forKey: "UserTunings")
        }
    }
    
    @Published var userTuningsUkulele: [String: [Float]] {
        didSet {
            UserDefaults.standard.set(userTuningsUkulele, forKey: "UserTuningsUkulele")
        }
    }
    
    @Published var userTuningsBass: [String: [Float]] {
        didSet {
            UserDefaults.standard.set(userTuningsBass, forKey: "UserTuningsBass")
        }
    }
    
    @Published var instrument: Instrument {
        didSet {
            UserDefaults.standard.set(instrument.rawValue, forKey: "Instrument")
        }
    }
    
    @Published var displayFrequency: FrequencyDisplayMode {
        didSet {
            UserDefaults.standard.set(displayFrequency.rawValue, forKey: "DisplayFrequency")
        }
    }
    
    @DisplayedNote var noteName: String
    @GaugePosition var position: Float
    
    var tuningFrequencies: [Float] {
        switch instrument {
        case .ukulele:
            return ukuleleDefaultTunings[self.currentTuningUkulele] ?? userTunings[self.currentTuningUkulele] ?? ukuleleDefaultTunings["Standard"]!
        case .bass:
            return bassDefaultTunings[self.currentTuningBass] ?? userTunings[self.currentTuningBass] ?? bassDefaultTunings["Standard"]!
        default:
            return defaultTunings[self.currentTuning] ?? userTunings[self.currentTuning] ?? defaultTunings["Standard"]!
        }
    }
    
    func getNoteName(frequency: Float) -> String {
        if self.chromaticMode {
            noteName = noteFromFrequency(frequency)
        } else {
            noteName = noteFromFrequency(closestFreqency(frequency, frequencies: tuningFrequencies))
        }
        
        return noteName
    }
    
    func getIndicatorColor(frequency: Float) -> Color {
        if frequency == 0.0 {
            return .secondary
        }
        
        if self.chromaticMode {
            return abs(frequency - closestFreqency(frequency)) < 0.5 ? .green : .red
        }
        
        return abs(frequency - closestFreqency(frequency, frequencies: tuningFrequencies)) < 0.3 ? .green : .red
        
    }
    
    func getGaugePosition(frequency: Float) -> Float {
        var ratio: Float
        
        
        if self.chromaticMode {
            ratio = frequency/closestFreqency(frequency)
        } else {
            ratio = frequency/closestFreqency(frequency, frequencies: tuningFrequencies)
        }
        
        if ratio.isNaN {
            return position
        }
        
        var halfWidth: Float = 154
        
        if UIDevice.current.localizedModel == "iPhone" {
            halfWidth = UIScreen.main.bounds.size.width > 350 ? 154 : 118
        } else if UIDevice.current.localizedModel == "iPad" {
            halfWidth = 308
        }
        
        position =  halfWidth*24*log2(ratio)
        
        return position
        
    }
    
    func getCents(frequency: Float) -> Int {
        var ratio: Float
        
        if frequency == 0 { return -9999 }
        
        if self.chromaticMode {
            ratio = frequency/closestFreqency(frequency)
        } else {
            ratio = frequency/closestFreqency(frequency, frequencies: tuningFrequencies)
        }
        
        return Int(1200*log2(ratio))
    }

    
    func getUserTuningsForCurrentInstrument() -> [String: [Float]] {
        switch self.instrument {
        case .ukulele:
            return self.userTuningsUkulele
        case .bass:
            return self.userTuningsBass
        default:
            return self.userTunings
        }
    }
    
    init() {
        self.currentTuning = UserDefaults.standard.object(forKey: "CurrentTuning") as? String ?? "Standard"
        self.currentTuningUkulele = UserDefaults.standard.object(forKey: "CurrentTuningUkulele") as? String ?? "Standard"
        self.currentTuningBass = UserDefaults.standard.object(forKey: "CurrentTuningBass") as? String ?? "Standard"
        
        self.chromaticMode = PurchasedProducts.shared.proFeatures ? (UserDefaults.standard.bool(forKey: "ChromaticMode")) : false
        
        self.userTunings =  UserDefaults.standard.object(forKey: "UserTunings") as? [String: [Float]] ?? [String: [Float]]()
        self.userTuningsUkulele =  UserDefaults.standard.object(forKey: "UserTuningsUkulele") as? [String: [Float]] ?? [String: [Float]]()
        self.userTuningsBass =  UserDefaults.standard.object(forKey: "UserTuningsBass") as? [String: [Float]] ?? [String: [Float]]()
        
        self.instrument = PurchasedProducts.shared.proFeatures ? (Instrument(rawValue: UserDefaults.standard.object(forKey: "Instrument") as? String ?? "Guitar") ?? Instrument.guitar) : Instrument.guitar
        self.displayFrequency = PurchasedProducts.shared.proFeatures ? (FrequencyDisplayMode(rawValue: UserDefaults.standard.object(forKey: "DisplayFrequency") as? Int ?? 0) ?? FrequencyDisplayMode.cents) : FrequencyDisplayMode.cents
    }
}
