//
//  AppDelegate.swift
//  Stroik
//
//  Created by Maksymilian Białas on 29/10/2020.
//

import Foundation
import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        StoreObserver.shared.startObserving()
        StoreObserver.shared.readProducts()
        StoreObserver.shared.validate(productIdentifiers: StoreObserver.shared.productIdentifiers)
        return true
    }
    func applicationWillTerminate(_ application: UIApplication) {
        StoreObserver.shared.stopObserving()
    }
}
