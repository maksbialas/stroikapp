//
//  Parameters.swift
//  Stroik
//
//  Created by Maksymilian Białas on 08/10/2020.
//

import Foundation

let fftSize: Int = 1024
let samplingRate: Float = 48000.0
let resultArrSize = 4


let defaultTunings: [String: [Float]] = [
    "Standard": [82.41, 110.00, 146.83, 196.00, 246.94, 329.63],
    "Drop D": [73.42, 110.00, 146.83, 196.00, 246.94, 329.63],
    "Half-Step Up": [87.31, 116.54, 155.56, 207.65, 261.63, 349.23],
    "Half-Step Down": [77.78, 103.83, 138.59, 185.00, 233.08, 311.13],
    "One Step Down": [73.42, 98.00, 130.81, 174.61, 220.00, 293.66],
    "Open C": [65.41, 98.00, 130.81, 196.00, 261.63, 329.63],
    "Open D": [73.42, 110.00, 146.83, 185.00, 220.00, 293.66],
    "Open E": [82.41, 123.47, 164.81, 207.65, 246.94, 329.63],
    "Open F": [87.31, 110.00, 130.81, 174.61, 261.63, 349.23],
    "Open G": [73.4, 98.00, 146.8, 196.0, 246.9, 293.7],
    "Open A": [82.41, 110.00, 164.81, 220.00, 261.63, 329.63],
    "DADGAD": [73.42, 110.00, 146.83, 196.00, 220.00, 293.66]
]

let tuningsOrdered = [
    "Standard",
    "Drop D",
    "Half-Step Up",
    "Half-Step Down",
    "One Step Down",
    "Open C",
    "Open D",
    "Open E",
    "Open F",
    "Open G",
    "Open A",
    "DADGAD"
]

let ukuleleDefaultTunings: [String: [Float]] = [
    "Standard": [392.00, 261.63, 329.63, 440.00],
    "Traditional": [440.00, 293.66, 369.99, 493.88],
    "Baritone": [146.83, 196.00, 246.94, 329.63]
]

let ukuleleTuningsOrdered = [
    "Standard",
    "Traditional",
    "Baritone"
]

let bassDefaultTunings: [String: [Float]] = [
    "Standard": [41.20, 55.00, 73.42, 98.00],
    "Drop D": [36.71, 55.00, 73.42, 98.00],
    "Half-Step Down": [38.89, 51.91, 69.30, 92.50],
    "One Step Down": [36.71, 49.00, 65.41, 87.31]
]

let bassTuningsOrdered = [
    "Standard",
    "Drop D",
    "Half-Step Down",
    "One Step Down",
]


let notes = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]


extension Array where Element: FloatingPoint {

    func sum() -> Element {
        return self.reduce(0, +)
    }

    func avg() -> Element {
        return self.sum() / Element(self.count)
    }

    func std() -> Element {
        let mean = self.avg()
        let v = self.reduce(0, { $0 + ($1-mean)*($1-mean) })
        return sqrt(v / (Element(self.count) - 1))
    }

}

