//
//  StroikApp.swift
//  Stroik
//
//  Created by Maksymilian Białas on 15/09/2020.
//

import SwiftUI

@main
struct StroikApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
