//
//  ContentView.swift
//  Stroik
//
//  Created by Maksymilian Białas on 15/09/2020.
//

import SwiftUI

var audioManage = StroikAudioManage(
    onRecordedCallback: audioCallback,
    samplingFrequency: samplingRate,
    detectionResult: DetectionResult(result: 0.0),
    bufferSize: 4096
)

struct ContentView: View {
    
    @State var showingSettings = false
    @StateObject var tunerDisplayState: TunerDisplayState = TunerDisplayState()
    
    var settingsButton: some View {
        Button(action: {
            self.showingSettings.toggle()
            
        }) {
            Image(systemName: "slider.horizontal.3")
                .imageScale(.large)
                .accessibility(label: Text("Settings"))
                .padding()
        }
    }

    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                settingsButton
            }
            TunerDisplay(detectionResult: audioManage.detectionResult, tunerDisplayState: tunerDisplayState)
        }
        .sheet(isPresented: $showingSettings) {
            SettingsView(showingSettings: self.$showingSettings, tunerDisplayState: tunerDisplayState)
        }
        .onAppear {
            UIApplication.shared.isIdleTimerDisabled = true
            audioManage.setupAudioUnit()
            audioManage.startRecording()
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
